package ru.bokhan.tm.listener;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import ru.bokhan.tm.event.ConsoleEvent;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractListener {

    @NotNull
    public abstract String command();

    @Nullable
    public abstract String argument();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent event);

}
