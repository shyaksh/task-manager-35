<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK EDIT</h1>
<form action="/task/edit/${task.id}/" method="post">
    <input type="hidden" name="id" value="${task.id}"/>
    <p>
    <div>NAME:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>
    <p>
    <div>DESCRIPTION:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>
    <button type="submit">SAVE TASK</button>
</form>
<jsp:include page="../include/_footer.jsp"/>
