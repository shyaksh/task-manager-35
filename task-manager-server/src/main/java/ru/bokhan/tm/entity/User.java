package ru.bokhan.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.enumerated.Role;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Task> tasks;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Project> projects;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Session> sessions;

    public User(
            @NotNull String id,
            @NotNull String login,
            @NotNull String passwordHash,
            @Nullable String email,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName,
            @Nullable Role role,
            @NotNull Boolean locked
    ) {
        super(id);
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.role = role;
        this.locked = locked;
    }

}