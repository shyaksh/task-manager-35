package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bokhan.tm.api.endpoint.ITaskEndpoint;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.TaskDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    ITaskService taskService;

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.create(session.getUserId(), projectId, name);
    }

    @Override
    @WebMethod
    public void createTaskWithDescription(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void addTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "task") @Nullable final TaskDTO task
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.add(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "task") @Nullable final TaskDTO task
    ) {
        if (session == null) throw new AccessDeniedException();
        if (task == null) return;
        sessionService.validate(session);
        taskService.removeById(session.getUserId(), task.getId());
    }

    @Override
    @WebMethod
    public void removeTaskAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        taskService.clear();
    }

    @Override
    @WebMethod
    public void removeTaskByUserAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<TaskDTO> findTaskAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        @Nullable final String userId = session.getUserId();
        return taskService.findAll(userId);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return taskService.findById(userId, id);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        taskService.updateByIndex(session.getUserId(), index, name, description);
    }

}
